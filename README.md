# tf_aws_account_guardduty

Configures GuardDuty for a Region

* Creates a GuardDuty detector
* Creates a GuardDuty invitation accepter
* Invites the detector to join the audit account's detector in this region

## Variables

| Name    | Type   | Description                                        | Default |
|---------|--------|----------------------------------------------------|---------|
| `email` | string | Email address (reguired by GuardDuty, but not used | _none_  |

## Outputs

None.

## Usage

This module must be instantiated for each region that requires GuardDuty.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "foo-prod"
}

module "guardduty_us_west_2" {
  source    = "git::https://code.stanford.edu/sc2_public/tf_aws_account_guardduty.git"
  email     = "aws-test37@lists.stanford.edu"
  providers = {
    aws       = aws.us_west_2
    aws.audit = aws.audit_us_west_2
  }
}
```

## Related Modules

* [AWS Default Setup](https://code.stanford.edu/sc2_public/tf_aws_account_default)
* [AWS Config setup](https://code.stanford.edu/sc2_public/tf_aws_account_config)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
