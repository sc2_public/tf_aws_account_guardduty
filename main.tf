#
# Set up GuardDuty in this region
#

provider "aws" {
  alias = "audit"
}

#
# create GuardDuty detector
#
resource "aws_guardduty_detector" "detector" {
  enable   = true
}

#
# invite this account into GuardDuty via the audit account
#
resource "aws_guardduty_member" "member" {
  provider                   = aws.audit
  account_id                 = data.aws_caller_identity.self.account_id
  detector_id                = data.aws_guardduty_detector.master.id
  email                      = var.email
  invite                     = true
  disable_email_notification = true
}

#
# Accept the invitation automatically
#
resource "aws_guardduty_invite_accepter" "accepter" {
  depends_on        = [aws_guardduty_member.member]
  detector_id       = aws_guardduty_detector.detector.id
  master_account_id = data.aws_caller_identity.audit.account_id
}

