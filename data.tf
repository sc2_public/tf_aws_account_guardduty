# identities and external resources

data "aws_caller_identity" "self" {}

data "aws_caller_identity" "audit" {
  provider = aws.audit
}

# master guardduty detector in this region
data "aws_guardduty_detector" "master" {
  provider = aws.audit
}

