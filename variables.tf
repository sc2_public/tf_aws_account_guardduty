variable "email" {
  description = "Email for AWS account - used for GuardDuty"
  type        = string
}
